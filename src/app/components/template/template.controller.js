/**
 * Created by jean-marc on 21/07/2016.
 * @module app/components/template/template.controller
 */
/*jshint esversion: 6*/
"use strict";

let $$window = null,
    $$PageService = null,
    $$scope = null;

export default class TemplateController {

    constructor(PageService, $scope, $window){

        $$window = $window;
        $$PageService = PageService;
        $$scope = $scope;
    }
    
    $onInit(){

        //adapt tpl-skin enabled features according
        //to the current page
        $$scope.$on("$locationChangeSuccess", this.update.bind(this));
    }
    
    update(){
        
        let page = $$PageService;

        //Set page title
        this._setTitle();

        /**
         * you can add as many as option that you want here
         * (just do not forget to set them on each route config
         * and their default value on PageProvider
         * @see app/components/hello/helloSettings
         * @see app/services/page/PageProvider
         */
        this.enableHeader = page.getOption("enableHeader");
        this.enableFooter = page.getOption("enableFooter");
    }
    
    _setTitle(){
        let title = $$PageService.getOption("title");

        $$window.document.title = title;
        this.command = title;
    }
}
TemplateController.$inject = ["PageService","$scope","$window"];