/**
 * Created by jean-marc on 21/07/2016.
 * @module app/components/template/template
 *
 * Template module definition. Here we can define
 * the template component and all transversal components
 * that may by associated to the template.
 */
/*jshint esversion: 6*/
"use strict";

import MODULE_NAME from "../../properties";

//import template style
import "./template.less!";

//import response grid
import "../../../assets/css/responsivegrid/col.css!";

import TemplateController from "./template.controller";
import templateTemplate from "./template.html!ng-template";

import progressbarDirective from "./progressbar/progressbar.directive";
import figureDirective from "./figure/figure.directive";

export default angular

    .module(MODULE_NAME.TEMPLATE, [])

    .component("appTemplate", Object.assign(
        templateTemplate,
        {
            controller: TemplateController
        }
    ))

    .directive("appProgressbar", progressbarDirective)

    .directive("figure", figureDirective);