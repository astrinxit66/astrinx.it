/**
 * Created by jean-marc on 09/08/2016.
 * @module app/components/template/progressbar/progressbarDirectiveSpec
 */
/*jshint esversion: 6*/
"use strict";

import mock from "angular-mocks";

import MODULE_NAME from "../../../properties";

describe("Progressbar directive", () => {
    let $rootScope = null, $compile = null, $interval = null;

    beforeEach(() => {
        mock.module(MODULE_NAME.TEMPLATE);

        mock.inject((_$rootScope_, _$compile_, _$interval_) => {
            $rootScope = _$rootScope_;
            $compile = _$compile_;
            $interval = _$interval_;
        });
    });

    it("should have correct proportion between progression and total", proportionTest);

    function proportionTest(){

        let scope = $rootScope.$new();

        let element = $compile(`<app-progressbar done="50" label="FT1"></app-progressbar>`)( scope );
        $interval.flush( Infinity );
        scope.$digest();

        expect( scope.$$childTail.donebar ).toBeDefined();

        expect( scope.$$childTail.donebar.length > 0 ).toBe( true );
        expect( scope.$$childTail.donebar.length ).toEqual( scope.$$childTail.undonebar.length );

        angular.element.cleanData( element );
        scope = $rootScope.$new();
        element = $compile(`<app-progressbar done="80" label="FT2"></app-progressbar>`)( scope );
        scope.$digest();

        let total = scope.$$childTail.undonebar.length;
        expect( total > 0 ).toBe( true );

        $interval.flush( Infinity );

        expect( scope.$$childTail.donebar.length ).toEqual( parseInt( total * 80 / 100 ) );
        expect( scope.$$childTail.undonebar.length + scope.$$childTail.donebar.length ).toEqual( total );

        angular.element.cleanData( element );
    }
});