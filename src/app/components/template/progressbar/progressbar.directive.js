/**
 * Created by jean-marc on 08/08/2016.
 * @module app/components/template/progressbar/progressbarDirective
 */
/*jshint esversion: 6*/
"use strict";

export default appProgressbar;

let $interval = null;

/**
 * A console like progressbar made with characters
 * @return {{link: progressbarLink, restrict: string, scope: {done: string, label: string}, template: string}}
 */
appProgressbar.$inject = ["$interval"];
function appProgressbar(_$interval_) {
    $interval = _$interval_;

    return {
        link: progressbarLink,
        restrict: "E",
        scope: {done: "@", label: "@"},
        template: `<div>|<span class="tpl-skin color enlighten">{{donebar}}</span><span style="color: #53585F">{{undonebar}}</span>|{{label}} (<span class="tpl-skin color enlighten">{{donelabel}}%</span>)</div>`
    };
}

progressbarLink.$inject = ["scope"];
function progressbarLink( scope ){
    let scale = 20,
        scaledPercentage = parseInt( parseInt( scope.done ) * scale / 100 );

    scope.donebar = scope.undonebar = "";
    scope.donelabel = 0;

    // fill progress bar with undone elements
    for(let i = 0; i < scale; i++){
        scope.undonebar += "=";
    }

    let counter = 0,
        interval = $interval(() => {
            if( counter === scaledPercentage ){
                _cancelInterval();
                return;
            }

            scope.donebar += "=";
            scope.undonebar = scope.undonebar.substring(0, scope.undonebar.length - 1);
            scope.donelabel += 100 / scale;
            counter++;
        }, 75);

    //////
    function _cancelInterval(){
        if( interval !== undefined ){
            $interval.cancel( interval );
            interval = undefined;
        }
    }
}