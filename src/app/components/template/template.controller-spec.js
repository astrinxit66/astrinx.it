/**
 * Created by jean-marc on 25/07/2016.
 * @module app/components/template/TemplateControllerSpec
 */
/*jshint esversion: 6*/
"use strict";

import mock from "angular-mocks";

import MODULE_NAME from "../../properties";

import "./template";
import "../../services/services";

const PARAMS = {
    TEST_CASE_1: {PATH: "/test", OPTIONS: {TITLE: "Template test config", HEADER: true, FOOTER: true}},
    TEST_CASE_2: {PATH: "/another-test", OPTIONS: {TITLE: "Template other test page", HEADER: true, FOOTER: false}}
};

describe("TemplateController", () => {

    beforeEach(() => {
        mock.module( MODULE_NAME.TEMPLATE, MODULE_NAME.SERVICES, _moduleConfig );

        function _moduleConfig( PageServiceProvider ){

            for(let testCase in PARAMS ){
                try { //silently skip duplicate exception
                    PageServiceProvider.register(PARAMS[ testCase ].PATH, {
                        title: PARAMS[ testCase ].OPTIONS.TITLE,
                        enableHeader: PARAMS[ testCase ].OPTIONS.HEADER,
                        enableFooter: PARAMS[ testCase ].OPTIONS.FOOTER
                    });
                } catch( e ){}
            }
        }
    });

    describe("1. Test enabled features", () => {
        let $ctrl = null, $location = null, $rootScope = null;

        beforeEach(mock.inject((_$rootScope_, $componentController, PageService, _$location_) => {

            $location = PageService._getLocationService() ? PageService._getLocationService() : _$location_;
            $rootScope = _$rootScope_;
            $ctrl = $componentController("appTemplate");
            $ctrl.$onInit();
        }));

        it("should have header and footer set according to PageService configuration", HeaderFooterEnabledTest);
        ////////

        function HeaderFooterEnabledTest(){

            for(let testCase in PARAMS){
                changeLocation( PARAMS[ testCase ].PATH );

                expect( window.document.title ).toEqual( PARAMS[ testCase ].OPTIONS.TITLE );
                expect( $ctrl.enableHeader ).toBe( PARAMS[ testCase ].OPTIONS.HEADER );
                expect( $ctrl.enableFooter ).toBe( PARAMS[ testCase ].OPTIONS.FOOTER );
            }
        }

        function changeLocation( path ){
            $location.path( path );
            $rootScope.$broadcast("$locationChangeSuccess");
        }
    });
});