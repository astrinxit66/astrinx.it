/**
 * Created by jean-marc on 08/08/2016.
 * @module app/components/template/img/imgDirective
 */
/*jshint esversion: 6*/
"use strict";

export default figureDirective;

/**
 * Add a vintage effect of slow loading to the DOM <figure> element
 * @return {{link: figureLink, restrict: string}}
 */
function figureDirective(){
    return {
        link: figureLink,
        restrict: "E"
    };
}

figureLink.$inject = ["scope","element"];
function figureLink(scope, element){
    let defaultCssClasses = "tpl-skin animate slide-down";

    element.addClass(`${defaultCssClasses} pending`);
    element.find("img").attr("onload", `this.parentElement.className = "${defaultCssClasses}"`);
}