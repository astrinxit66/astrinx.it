/**
 * Created by jean-marc on 09/08/2016.
 * @module app/components/template/figure/figureDirectiveSpec
 */
/*jshint esversion: 6*/
"use strict";

import mock from "angular-mocks";

import MODULE_NAME from "../../../properties";

describe("Figure directive", () => {
    let element = null, scope = null;

    beforeEach(() => {
        mock.module(MODULE_NAME.TEMPLATE);

        mock.inject(($rootScope, $compile) => {
            scope = $rootScope.$new( true );

            element = $compile(`<figure><img src="http://fake-url.com/img1.png"></figure>`)( scope );
            scope.$digest();
        });
    });

    afterEach(() => {
        angular.element.cleanData( element );
        element = null;
    });

    it("should add the needed classes onto the style attr of the figure element", cssClassAddingTest);
    ///////

    function cssClassAddingTest(){

        expect(
            element.hasClass("tpl-skin") &&
            element.hasClass("animate") &&
            element.hasClass("slide-down")
        ).toBe( true );

        expect( element.find("img").attr("onload") ).toBeDefined();
    }
});