/**
 * Created by jean-marc on 08/08/2016.
 * @module app/components/components
 *
 * Here are imported our components
 */
/*jshint esversion: 6*/

/**
 * Template component
 * This is part of the proposed feature
 * and should not be removed then.
 * (not an example)
 */
import "./template/template";

import "./whoami/whoami";