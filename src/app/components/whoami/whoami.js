/**
 * Created by jean-marc on 08/08/2016.
 * @module app/components/whoami/whoami
 */
/*jshint esversion: 6*/
"use strict";

import MODULE_NAME from "../../properties";

import whoamiSettings from "./whoami.settings";

import WhoamiController from "./whoami.controller";
import whoamiTemplate from "./whoami.html!ng-template";

import "../../../assets/css/responsivegrid/3cols.css!";

export default angular

    .module(MODULE_NAME.WHOAMI, [
        "ngRoute"
    ])

    .component("whoami", Object.assign(
        whoamiTemplate,
        {
            controller: WhoamiController
        }
    ))

    .config( whoamiSettings );