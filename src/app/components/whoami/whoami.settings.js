/**
 * Created by jean-marc on 08/08/2016.
 * @module app/components/whoami/WhoamiSettings
 */
/*jshint esversion: 6*/
"use strict";

export default whoamiSettings;

whoamiSettings.$inject = ["$routeProvider", "PageServiceProvider"];
function whoamiSettings($routeProvider, PageServiceProvider){
    let locationPath = "/whoami";
    
    PageServiceProvider.register(locationPath, {
        title: "whoami"
    });
    
    $routeProvider
        .when(locationPath, {
            template: "<whoami></whoami>"
        })

        .otherwise({
            redirectTo: locationPath
        });
}