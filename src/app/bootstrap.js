/**
 * Created by jean-marc on 21/07/2016.
 * @module app/bootstrap
 *
 * We do not use ngApp anymore as our files
 * are loaded asynchronously. This file is
 * in charge of bootstraping the application
 * when angular is ready.
 */
/*jshint esversion: 6*/
"use strict";

//js vendor deps
import angular from "angular";
import "angular-route";

//app components are referenced into
// components/components.js
import "./components/components";

/**
 * We are providing our app with an end to end
 * mock support. The e2emock module is then
 * the module that bootstrap our app. The
 * real app is loaded as e2emock dependency.
 * You can/should/have to replace this by
 * the real app when you do not need to use
 * e2emock anymore. That is the case when your
 * backend app is ready to serve data to the frontend
 * or in production mode.
 * You can easily enable the real app by uncommenting
 * the following line (do not forget to comment the second
 * one then)
 */
//import mainModule from "./app";
import mainModule from "../e2emock/e2emock";

angular.element(document).ready(() => {

    angular.bootstrap(document, [mainModule.name], {

        //prefer strict dependency injection mode
        //for minification
        strictDi: true
    });
});