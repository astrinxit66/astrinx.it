/**
 * Created by jean-marc on 21/07/2016.
 *
 * The app module is where you declare the 
 * main application module. Here our pages 
 * are modules that are injected to the app
 * as module dependencies.
 *
 * @module app/app
 */
/* jshint esversion: 6*/
"use strict";

import "./services/services";

import MODULE_NAME from "./properties";

export default angular

    .module(MODULE_NAME.APP, [
        //our app will need our services
        MODULE_NAME.SERVICES,

        //app components (pages) should
        //be listed here as module deps
        MODULE_NAME.TEMPLATE,
        MODULE_NAME.WHOAMI
    ]);