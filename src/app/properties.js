/**
 * Created by jean-marc on 21/07/2016.
 * @module app/properties
 *
 * Here stands global properties that aims to be reused
 * along the application
 */
/*jshint esversion: 6*/
"use strict";

/** @namespace */
const MODULE_NAME = {};

export default MODULE_NAME;

MODULE_NAME.APP = "xit";
MODULE_NAME.SERVICES = `${MODULE_NAME.APP}.services`;
MODULE_NAME.TEMPLATE = `${MODULE_NAME.APP}.template`;

// components module
MODULE_NAME.WHOAMI = `${MODULE_NAME.APP}.whoami`;