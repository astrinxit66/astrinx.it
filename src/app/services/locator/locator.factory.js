/**
 * Created by jean-marc on 21/07/2016.
 *
 * The locatorFactory handle all the request url
 * that can be used through the app. This is preferable
 * for urls' maintainability and minification where there
 * is only one string value per request
 * @module app/services/locator/locatorFactory
 */
/*jshint esversion: 6*/
"use strict";

export default function locatorFactory(){

    let uri = "http://some-backend-service.app",
        urls = {};

    // Example of an url definition
    urls.hello = `${uri}/hello`;

    return urls;
}