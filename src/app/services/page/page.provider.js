/**
 * Created by jean-marc on 23/07/2016.
 * @module app/services/page/PageProvider
 *
 */
/*jshint esversion: 6*/
"use strict";

import PageService from "./page.service";

let $$config = [];

export default class PageProvider {
    
    get $get(){
        PageService.$config = $$config;

        return ["$location","$q","$interval", PageService.instance];
    }

    /**
     * Allow a module to register a page. Registered pages
     * are tracked by the Template so that it can adapt its
     * presentation to the page. You can for example enable
     * in each registered page the tpl-skin options that you
     * want to be shown for the specified page (eg: header,
     * footer, your custom options, etc.).
     *
     * @param locationPath:string
     * @param options:object
     */
    register(locationPath, options){

        //ensure requirements
        this._requires("location path", locationPath);
        this._requires("options object", options, "title");

        // default options
        options.enableHeader = options.enableHeader !== undefined ? options.enableHeader : true;
        options.enableFooter = options.enableFooter !== undefined ? options.enableFooter : true;

        this._shouldNotBeRegistered( locationPath );

        $$config.push(Object.assign({path: locationPath}, options));
    }

    _requires(objectName, objectValue, property){

        if( !objectValue ){
            throw new TypeError(`Expecting ${objectName} to be defined`);
        }

        if( property !== undefined && !objectValue.hasOwnProperty( property ) ){
            throw new TypeError(`Expecting ${objectName} to have ${property} property`);
        }
    }

    _shouldNotBeRegistered( locationPath ){

        if( $$config.find((record) => record.path === locationPath) !== undefined ){
            throw new Error(`${locationPath} already registered`);
        }
    }
}