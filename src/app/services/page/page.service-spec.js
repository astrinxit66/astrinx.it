/**
 * Created by jean-marc on 03/08/2016.
 * @module app/services/page/PageServiceSpec
 */
/*jshint esversion: 6*/
"use strict";

import mock from "angular-mocks";

import MODULE_NAME from "../../properties";

import "../services";

const PAGE_DEFINITION = {
    title: "Page title",
    enableHeader: true,
    enableFooter: true
};

describe("PageService", () => {

    beforeEach(() => {
        mock.module(MODULE_NAME.SERVICES, _configModule);

        function _configModule( PageServiceProvider ){
            try { //silently skip duplicate exception
                PageServiceProvider.register("/fake-path", PAGE_DEFINITION);
                PageServiceProvider.register("/fake-path-2", PAGE_DEFINITION);
            } catch( e ){}
        }
    });

    describe("1. Check configuration", () => {

        it("should have access to the data set by the service provider", mock.inject( dataAccessTest ));
        it("should not expose data set by the service provider", mock.inject( dataExpositionTest ));
        //////

        function dataAccessTest( PageService ){
            let locationPath = "/fake-path";

            expect( PageService.getOptions( locationPath ) ).toBeNonEmptyObject();
            expect( PageService.getOption("title", locationPath) ).toEqual( PAGE_DEFINITION.title );
            expect( PageService.getOption("enableHeader", locationPath) ).toBe( PAGE_DEFINITION.enableHeader );
            expect( PageService.getOption("enableFooter", locationPath) ).toBe( PAGE_DEFINITION.enableFooter );
        }

        function dataExpositionTest( PageService ){
            expect( PageService.$config ).toBe( undefined );
        }
    });
});