/**
 * Created by jean-marc on 23/07/2016.
 * @module app/services/page/PageService
 */
/*jshint esversion: 6*/
"use strict";

let $$interval = null,
    $$location = null,
    $$pageOptions = null,
    $$q = null,
    $$serviceInstance = null;

export default class PageService {

    static instance(...dependencies) {

        if (!($$serviceInstance instanceof PageService)) {
            $$serviceInstance = new PageService(...dependencies);
        }

        return $$serviceInstance;
    }

    constructor($location, $q, $interval){

        $$location = $location;
        $$q = $q;
        $$interval = $interval;

        // privatize the $config property received from
        // PageProvider
        $$pageOptions = PageService.$config;
        delete PageService.$config;
    }

    getOptions(locationPath = null){
        let options = $$pageOptions.filter((row) => row.path === (locationPath === null ? $$location.path() : locationPath));

        return options.length > 0 ? options.shift() : undefined;
    }

    getOption(option, locationPath = null){
        let currentPageOptions = this.getOptions( locationPath );

        return !currentPageOptions || !currentPageOptions.hasOwnProperty( option ) ? undefined : currentPageOptions[ option ];
    }

    /**
     * For UT purpose
     * @private
     */
    _getLocationService(){
        return $$location;
    }

    /**
     * Add a ready flag on the registered page corresponding
     * to the given locationPath. Use the current location if
     * locationPath is not provided.
     * The ready flag can serves anyone, it is just a state of
     * the readiness of the page. This method can be called when
     * $routeProvider finished to resolve data for the current
     * page for example.
     * @see PageService.whenReady
     *
     * @param locationPath:{string|null}
     */
    ready( locationPath = null ){
        let path = locationPath !== null ? locationPath : $$location.path();

        this.getOptions( path ).$ready = true;
    }

    /**
     * Used in conjunction of PageService.ready, this offers the
     * ability to track the ready state of a page. It returns a
     * promise that is resolved when the ready flag is set to true.
     *
     * @return {Promise}
     */
    whenReady( locationPath = null ){

        //noinspection JSValidateTypes
        let deferred = $$q.defer();

        let intervalId = $$interval(() => {
            if(this.getOption("$ready", locationPath) === true){
                deferred.resolve();
                _cancelInterval();
            }
        }, 300);

        function _cancelInterval(){
            if( intervalId !== undefined ){
                $$interval.cancel( intervalId );
                intervalId = undefined;
            }
        }

        return deferred.promise;
    }
}