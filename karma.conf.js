/**
 * Created by jean-marc on 22/07/2016.
 */
"use strict";

/**
 * Created by jean-marc on 24/02/2016.
 */
module.exports = function(config) {
    config.set({

        basePath: "./",
        frameworks: ["systemjs", "jspm", "jasmine", "jasmine-matchers"],

        files: [
            "jspm_packages/npm/core-js@1.2.7/es6.js",
            "node_modules/phantomjs-polyfill-object-assign/object-assign-polyfill.js",
            "src/**/*-spec.js"
        ],

        jspm: {
            serveFiles: [
                "src/**/*.js",
                "src/**/*.html",
                "src/**/*.less",
                "src/**/*.css"
            ]
        },

        preprocessors: {
            "src/**/*.js": ["babel"]
        },

        systemjs: {
            configFile: "./config.js",

            serveFiles: [
                "node_modules/less/dist/less.min.js"
            ],

            config: {
                paths: {
                    "lesscss": "node_modules/less/dist/less.min.js"
                }
            }
        },

        plugins: [
            "karma-systemjs",
            "karma-jspm",
            "karma-babel-preprocessor",
            //"karma-chrome-launcher", //to be enabled if you want a browser debug console
            "karma-phantomjs-launcher",
            "karma-jasmine",
            "karma-jasmine-matchers",
            "karma-spec-reporter"
        ],

        reporters: ["spec"],

        specReporter: {
            maxLogLines: 5,
            suppressErrorSummary: false,
            suppressFailed: false,
            suppressPassed: false,
            suppressSkipped: false
        },

        port: 7357,

        color: true,

        logLevel: config.LOG_ERROR,

        autoWatch: false,

        browsers: [
            //"Chrome", //to be enabled if you want a browser debug console
            "PhantomJS"
        ],

        singleRun: true //to be disabled if you want the browser to not auto-close
    });
};