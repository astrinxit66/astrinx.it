/**
 * Created by jean-marc on 20/07/2016.
 *
 * The purpose of package tasks are to set a runnable-ready
 * version of the project.
 *
 * For package:dev task, it creates
 * an isolated temporary folder where it put a symbolic link of the app
 * and the necessary files for that app to be able to ran onto a browser.
 * The resulted package aims to be used for functional tests and for debug.
 *
 * For package:prod task, it creates an isolated folder where it create a
 * copy of the app and build it (concat, minify, less to css, etc.) in order
 * for it to be able to ran as a standalone app onto a browser. The resulted
 * package is the one that can be deployed.
 */

(function(){
    "use strict";

    module.exports = packageInitializer;

    var
        //shortcut for plugins.config
        config = null,

        //shortcut for plugins.gulp
        gulp = null,

        //gulp plugin container, we deliberately list
        //the plugin components that we are using in this
        //file for code clarity
        plugins = {
            config: null,
            gulp: null,
            htmlReplace: null,
            mergeStream: null,
            notify: null,
            symlink: null
        };

    function packageInitializer( pluginCollection ){
        config = pluginCollection.config;
        gulp = pluginCollection.gulp;
        plugins = pluginCollection;

        //register package tasks
        gulp.task("package:dev", ["clean:dev"], packageDevTask);
        gulp.task("package:prod", packageProdTask);
    }

    function packageDevTask(){

        var
            //copy the index.html file into the dev dist folder and replace
            //the blocks
            pkgIndexHtml = gulp.src( config.path.src +"/index.html" )
                .pipe( plugins.htmlReplace(
                    {
                        "systemjs": ["less.min.js", "jspm_packages/system.js", "config.js"]
                    },
                    {
                        keepUnassigned: true
                    }
                ))
                .pipe(gulp.dest( config.path.dist.dev )),

            //create a symbolic link of the e2emock folder to the dev dist folder
            linkE2EMock = gulp.src( config.path.src +"/e2emock" )
                .pipe(plugins.symlink( config.path.dist.dev +"/e2emock" )),

            //create a symbolic link of the less processor file to de the dev dist folder.
            //This allow less files in the project to be parsed at runtime for dev test purposes
            linkLessProcessor = gulp.src( config.less.processorPath )
                .pipe( plugins.symlink( config.path.dist.dev +"/less.min.js" )),

            //create a symbolic link of the jspm packages folder to allow the isolated project
            //to have access to its dependencies
            linkJspmPackages = gulp.src( config.path.project +"jspm_packages" )
                .pipe(plugins.symlink( config.path.dist.dev +"/jspm_packages" )),

            //create a symbolic link of the SystemJs config file to the dev dist folder
            linkSystemjsConfigFile = gulp.src( config.path.project +"config.js" )
                .pipe(plugins.symlink( config.path.dist.dev +"/config.js")),

            //create a symbolic link of the assets folder to the dev dist folder
            linkAssets = gulp.src( config.path.src +"/assets" )
                .pipe(plugins.symlink( config.path.dist.dev +"/assets")),

            //create a symbolic link of the app folder to the dev dist folder
            linkApp = gulp.src( config.path.app )
                .pipe(plugins.symlink( config.path.dist.dev +"/app"));

        return plugins.mergeStream(
            pkgIndexHtml,
            linkE2EMock,
            linkLessProcessor,
            linkJspmPackages,
            linkSystemjsConfigFile,
            linkAssets,
            linkApp,
            gulp.src("").pipe( plugins.notify("Done packaging app") )
        );
    }
    
    function packageProdTask(){
        
    }
})();