/**
 * Created by jean-marc on 19/07/2016.
 */

(function(){
    "use strict";

    /**
     * the gulpBootstrap init the serve and test tasks
     * by providing them a set of gulp plugin
     * @type {gulpBootstrap}
     */
    module.exports = gulpBootstrap;

    /**
     * Here are listed the available gulp tasks.
     * A task file may define multiple tasks that
     * are within the context of the file
     * (eg: serve.task.js contains a serve:dev and
     * a serve:prod tasks)
     * @type {*[]}
     */
    var taskCollection = [
        require("./clean.task"),
        require("./package.task"),
        require("./serve.task"),
        require("./test.task"),
        require("./validate.task")
    ];

    /**
     * Here are the plugin list that gulp tasks may
     * use
     * @type {{}}
     */
    var pluginCollection = {
        config: require("./gulp.config"),
        connect: require("gulp-connect"),
        defaultBrowser: _resolveBrowserApp(),
        del: require("del"),
        gulp: null,
        htmlReplace: require("gulp-html-replace"),
        jshint: require("gulp-jshint"),
        jshintStylishReporter: require("jshint-stylish"),
        karmaServer: require("karma").Server,
        mergeStream: require("merge-stream"),
        notify: require("gulp-notify"),
        open: require("opn"),
        symlink: require("gulp-symlink")
    };

    function gulpBootstrap( gulpInstance ){

        //register the gulp instance into the plugin list
        pluginCollection.gulp = gulpInstance;

        //init each task
        for(var i = 0; i < taskCollection.length; i++){
            var task = taskCollection[ i ];

            task( pluginCollection );
        }
    }

    function _resolveBrowserApp(){
        /** @type Os */
        var os = require("os");

        return os.platform() === "linux" ? "google-chrome"
            : (
            os.platform() === "darwin" ? "google chrome"
                : (
                os.platform() === "win32" ? "chrome"
                    : "firefox"
            )
        );
    }
})();