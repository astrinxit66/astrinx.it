/**
 * Created by jean-marc on 19/07/2016.
 */

(function(){
    "use strict";

    var config = module.exports = {};

    /**
     * config for gulp-connect
     * @type {{prod, dev}|*}
     */
    config.connect = connectConfig();

    /**
     * config.path contains the project relevant paths
     * that are manipulated by our gulp tasks
     */
    config.path = pathConfig();

    /**
     * config for gulp-jshint
     */
    config.jshint = jshintConfig();

    /**
     * config for less
     */
    config.less = lessConfig();

    //////////////

    function connectConfig(){
        return {
            prod: {
                livereload: true,
                port: 9503
            },
            dev: {
                livereload: true,
                port: 9000
            }
        };
    }

    function jshintConfig(){
        return {
            rcfile: config.path.project +".jshintrc",
            reportMode: "fail"
        };
    }

    function lessConfig(){
        return {
            includePaths: { paths: ["../src/app", "../vendor/style", __dirname] },
            processorPath: config.path.project +"node_modules/less/dist/less.min.js"
        };
    }

    function pathConfig(){
        var projectPath = __dirname +"/../";

        return {
            app: projectPath +"src/app",
            project: projectPath,
            src: projectPath +"src",
            subfiles: "/**/*",

            dist: {
                dev: projectPath +"tmp",
                prod: projectPath +"dist"
            }
        };
    }
})();