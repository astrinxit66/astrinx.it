/**
 * Created by jean-marc on 19/07/2016.
 */

(function(){
    "use strict";

    module.exports = cleanInitializer;


    var
        //shortcut for plugins.config
        config = null,

        //shortcut for plugins.gulp
        gulp = null,

        //gulp plugin container, we deliberately list
        //the plugin components that we are using in this
        //file for code clarity
        plugins = {
            config: null,
            del: null,
            gulp: null,
            mergeStream: null
        };

    function cleanInitializer( pluginCollection ){
        config = pluginCollection.config;
        gulp = pluginCollection.gulp;
        plugins = pluginCollection;

        //register clean tasks
        gulp.task("clean:dev", cleanTask.bind(undefined, "dev"));
        gulp.task("clean:prod", cleanTask.bind(undefined, "prod"));
    }

    function cleanTask(){
        var folder = arguments[ 0 ];

        return plugins.del.sync( config.path.dist[ folder ] );
    }
})();