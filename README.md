# [astrinx.it](http://astrinx.it)

This is my personal website built based on my [angular-starter/1.5.7-es6 project](https://bitbucket.org/astrinxit66/angular-starter/src/5991e7f0047e/1.5.7-es6/?at=master).

## Quick installation

The process of installing this project is quite the same as for [angular-starter/1.5.7-es6 project](https://bitbucket.org/astrinxit66/angular-starter/src/5991e7f0047e/1.5.7-es6/?at=master). Just replace the angular-starter git repository url by `https://bitbucket.org/astrinxit66/astrinx.it.git` when you're on the cloning step.

## Serving

Same process as [angular-starter/1.5.7-es6 project](https://bitbucket.org/astrinxit66/angular-starter/src/5991e7f0047e/1.5.7-es6/?at=master).